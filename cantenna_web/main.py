from flask import Flask
from flask import Response
from flask import render_template
from flask import request
from flask_sock import Sock
from flask_sock import ConnectionClosed
import json

import smbus
import RPi.GPIO as GPIO

from cantenna_web.spmt_switch import SP8T_switch, InvalidThrowError
from cantenna_web.mcp23017 import MCP23017
from cantenna_web.mcp23017 import GPIOPort as MCPPort
from cantenna_web.band_data import BandData

INT_BAND_DATA_PIN = 15
BAND_DATA_PORT = MCPPort.A

BAND_SWITCH_MAP = {
    "160": 0,
    "80": 0,
    "40": 1,
    "30": 0,
    "20": 2,
    "17": 0,
    "15": 3,
    "12": 0,
    "10": 4,
    "6": 4,
}



# MCP23017 for sensing band data
i2cbus = smbus.SMBus(1)

mcp23017 = MCP23017(i2cbus, 0x21)
mcp23017.set_iodir(BAND_DATA_PORT, 0x0F) # Pins 0-3 input
mcp23017.configure_interrupt(port=BAND_DATA_PORT,
                             control=0x00,
                             gp_enable=0x01)
band_data = BandData(mcp23017, MCPPort.A, [3, 2, 1, 0])
mcp23017.read_port(MCPPort.A)

# MCP23017 interrupt
GPIO.setmode(GPIO.BOARD)
GPIO.setup(INT_BAND_DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


# Antenna switching functions
def set_switch_and_notify(pole):

    spmt_switch.set_active_throw(pole)

    html = render_switch_panel()
    for ws in websocket_registry:
        ws.send(html)

def bandswitch_callback(channel):
    value = band_data.read_band()
    print("Band switching to {value}")
    set_switch_and_notify(BAND_SWITCH_MAP[value])

GPIO.add_event_detect(INT_BAND_DATA_PIN,
                      GPIO.FALLING,
                      callback=bandswitch_callback)
    
# SMPT Switch
spmt_switch = SP8T_switch("switch0")


# Flask app
app = Flask(__name__)
app.logger.setLevel(10)
app.config["SOCK_SERVER_OPTIONS"] = {"ping_interval": 25}
sock = Sock(app)

websocket_registry = []


# Web app routes and methods
@app.route("/")
def index():
    return render_template("switch.html")


def render_switch_panel():
    with app.app_context():
        return render_template("switch-array.html", switch=spmt_switch)


@app.route("/api/v1/set-switch", methods=["POST"])
def api_set_switch():

    try:
        set_switch_and_notify(request.form["throw"])
    except InvalidThrowError:
        return (
            render_template(
                "switch_bad_throw.html",
                requested_throw=request.form["throw"],
                switch=spmt_switch,
            ),
            418,
        )
    except KeyError:
        return (
            render_template(
                "bad_request.html", message="Form data 'throw' not received"
            ),
            400,
        )

    return "OK", 200


@sock.route("/ws")
def sock_set_switch(sock):

    app.logger.info("Opening socket %s", sock)

    # Create socket and send relevant data to the
    # new listener.
    websocket_registry.append(sock)
    sock.send(render_switch_panel())

    try:
        while True:
            msg = sock.receive()
            data = json.loads(msg)
            app.logger.info(
                "Manual switch from %s to pole %s", sock, data["pole"]
            )
            set_switch_and_notify(int(data["pole"]))

    except ConnectionClosed:
        app.logger.info("Closing socket %s", sock)
        websocket_registry.remove(sock)
