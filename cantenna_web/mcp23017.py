"""Test the MCP23017 class"""

from enum import Enum
from smbus import SMBus

IODIR = 0x00
OLAT = 0x14
GPIO = 0x12
INTCON = 0x08
GPINTEN = 0x04
INTCAP = 0x10

class GPIOPort(Enum):
    A = 0x00
    B = 0x01

class MCP23017:

    def __init__(self, bus: SMBus, device_address: int):
        """Creates an MCP23017 driver object

        :param bus: the SMBus on which the MCP23017 device resides
        :param device_address: the address (hex) of the device on the SMBus
        """
        self.bus = bus
        self.device_address = device_address

    def set_iodir(self, port: GPIOPort, value: int):
        """Sets the IO direction register

        :param port: the port (A or B)
        :param value: the value (8-bit binary) to set the direction
        """
        self.bus.write_byte_data(self.device_address, IODIR+port.value, value)

    def configure_interrupt(self, port: GPIOPort, control: int, gp_enable: int):
        """Configures the device interrupt
        
        :param port: the GPIO port (A or B)
        :param control: an 8-bit binary value to set the INTCON register
        :param gp_enable: an 8-bit mask to enable interrupts on respective 
                          port pins
        """
        self.bus.write_byte_data(self.device_address, INTCON+port.value, control)
        self.bus.write_byte_data(self.device_address, GPINTEN+port.value, gp_enable)

    def read_port(self, port: GPIOPort) -> int:
        return self.bus.read_byte_data(self.device_address, GPIO+port.value)

    def clear_interrupt(self, port: GPIOPort):
        self.bus.read_byte_data(self.device_address, INTCAP+port.value)
