"""Converts port data to Yaesu band data output"""

__all__ = ["BandData"]

MANUFACTURER_BAND_MAPS = {
    "yaesu": {
        0b0001: "160",
        0b0010: "80",
        0b0011: "40",
        0b0100: "30",
        0b0101: "20",
        0b0110: "17",
        0b0111: "15",
        0b1000: "12",
        0b1001: "10",
        0b1010: "6",
    }
}


def is_set(bit, value):
    return (value & bit) == bit


def genkey(bcd_value, pins):
    """Returns an integer representing `bcd_value` with the
       bits re-shuffled according to `pins`"""

    return (
        (is_set(0b1000, bcd_value) << pins[0])
        | (is_set(0b0100, bcd_value) << pins[1])
        | (is_set(0b0010, bcd_value) << pins[2])
        | (is_set(0b0001, bcd_value) << pins[3])
    )


class BandData:
    def __init__(self, io_driver, port, pins, bcd_version="yaesu"):
        """Constructs a BCD decoder class

        :param driver: a driver object with a `driver.read` method
                       that returns an `int` giving the value of a
                       GPIO port.
        :param port: a port identifier
        :param pins: a list mapping port pin ids to bits in the BCD.  Pins are
                     arranged in order of _decreasing_ bit significance.
        :param bcd_version: the BCD version to use.  Currently only "yaesu" is
                            supported.
        """
        self.driver = io_driver
        self.mcp23017_port = port
        self.pins = pins
        self.bcd_version = bcd_version

        self._make_band_map()

    def _make_band_map(self):

        try:
            manufacturer_band_map = MANUFACTURER_BAND_MAPS[self.bcd_version]
        except KeyError as key_error:
            raise KeyError(
                f"Manufacturer `{self.bcd_version}` is not supported"
            ) from key_error

        band_map = {}
        for i in range(16):
            try:
                band = manufacturer_band_map[i]
                key = genkey(i, self.pins)
                band_map[key] = band

            except KeyError:
                pass

        self.band_map = band_map

    def _read_raw_port_data(self):
        """Returns the raw data from the underlying GPIO port
           connected to the rig"""
        return self.driver.read_port(self.mcp23017_port)

    def read_band(self):
        """Returns the currently mapped frequency band (in metres)"""
        raw_port = self._read_raw_port_data()
        print("Band map:", self.band_map)
        return self.band_map[raw_port]
